# My Personal Website

## Author
Andrew Miracle

## Project Description


## Built with
* [Atom] (https://atom.io) - A hackable text editor
for the 21st Century
* [Github] (https://github.com) - Github
* [Bootstrap 3.3.7] (http://blog.getbootstrap.com/) - Github
* [Font Awesome 4.6.3 by @davegandy] (http://fontawesome.io)


## Project Setup
To setup this project on your local computer download the master branch using this link https://gitlab.com/tecmie/personal/archive/master.zip or run the command on your PC if you have git installed

`git clone https://gitlab.com/tecmie/personal`


### Requirements
* Git installed on Computer
* Web Browser, preferably Mozilla Firefox
* To edit Notepad Gedit or Text Editor


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
